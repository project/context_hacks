(function($){

Drupal.behaviors.aaaContextHacksReactionBlock = { 
  attach: function(context) {

    // Add soft selection after removal
    $('#context-blockform a.remove:not(.processed-hacks)').addClass('processed-hacks').each(function() {
      $(this).click(function() {
        var elem = $(this).parents('tr').eq(0);
        $('div.context-blockform-selector input[value='+elem.attr('id')+']').attr('checked', 'checked').css('opacity', 0.3);
        $('div.context-blockform-selector input[type=checkbox]').bind('click.blockRemove', function() {
          $('div.context-blockform-selector input[value='+elem.attr('id')+']').removeAttr('checked').css('opacity', 1);
          $('div.context-blockform-selector input[type=checkbox]').unbind('click.blockRemove');
        });
      });
    });

    // Add search
    $('#context-blockform .context-blockform-selector:not(.processed-hacks)').addClass('processed-hacks').parent().prepend('<input type="search" id="search-filter-blocks" name="foo" />');
    var timer = null;
    $('#search-filter-blocks:not(.processed-hacks)').addClass('processed-hacks').bind('keyup change click', function() {
      var val = $(this).val();
      window.clearTimeout(timer);
      timer = window.setTimeout( function() {
        var elems = $('div.context-blockform-selector label.option:contains("' + val + '")');
        $('div.context-blockform-selector label.option').removeClass('search-hidden');
        Drupal.contextBlockForm.setState();
        $('div.context-blockform-selector label.option').parent().filter(':visible').hide().addClass('search-hidden');
        if (elems.length > 0) {
          $(elems).parent().filter('.search-hidden').show();
        }
      }, 300);
    });
  }
};

/* HACK: Workaround a bug in context ui - very hacky ... */

Drupal.behaviors.zzzContextHacksReactionBlock = {
  attach: function(context) {
    $('#context-blockform:not(.processed-hacks)', context).addClass('processed-hacks').each(function() {

      // Use the same height as the blocks section if > 240px
      $('.context-plugin-form-block').addClass('active-form');
      var blocks_height = $('#context-blockform').find('td.blocks').height();
      if (blocks_height > 240) {
        // but not higher than 400px
        if (blocks_height > 400) {
          blocks_height=400;
        }
        $('#context-blockform td.selector').find('.context-blockform-selector').css('height', blocks_height + 'px');
      }
      $('.context-plugin-form-block').removeClass('active-form');

      // Fix bug by removing handler for non add-block links
      var x = $(this);
      window.setTimeout(function() {
        $(x).
          find('td.blocks a:not(.add-block)').
          unbind('click').
          removeClass('processed').
          removeClass('processed-hacks');
        Drupal.attachBehaviors(context);
      }, 500);
    });
  }
};

})(jQuery);
